.PHONY: install uninstall set-default

uninstall:
	rm -rv /usr/share/plymouth/themes/solar-schools || true

install: uninstall
	mkdir /usr/share/plymouth/themes/solar-schools
	cp -v *.script *.plymouth *.png /usr/share/plymouth/themes/solar-schools/

set-default:
	plymouth-set-default-theme -R solar-schools
