### Installation

    wget https://gitlab.com/solarschools/plymouth-loaders/solar-schools/-/archive/master/solar-schools-master.tar.gz
    tar xf solar-schools-master.tar.gz
    cd solar-schools-master
    sudo make install   # as root or with sudo
    cd ..
    rm -rf solar-schools-master* 
    sudo plymouth-set-default-theme solar-schools
    # reboot to see the new theme

### License

This theme is licensed under GPLv2, for more details check COPYING.
